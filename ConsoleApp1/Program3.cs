using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vehical
{
    // It is a parent class for the child Car and Bike.

    class Vehical
    {
        private string _name = "";
        string Color = "";
        int WheelNo = 0;
        int MaxSpeed = 0;

        public Vehical()
        {
            Console.WriteLine("Vehical has started...");

        }

        public void CalculateTolltax(int Tax)
        {
            Console.WriteLine("Toll Tax for your vehical is: " + Tax*(100-8));
        }

        public void Vehicals(string Name, string Color, int WheelNo, int MaxSpeed)
        {
            _name = Name;
            Color = color;
            WheelNo = WheelNo;
            MaxSpeed = MaxSpeed;

            Console.WriteLine("Name of your vehical is: " + _name);
            Console.WriteLine("Color of your vehical is: " + Color);
            Console.WriteLine("Total wheel in your vehical is : " + WheelNo);
            Console.WriteLine("Maximum speed of your vehical is: " + MaxSpeed);

        }

    }


    // Here Bike child is inheriting the property of the parent Vehical.
    //   Here, the keyword "sealed" insures that no other class can be derived by this 
    //   using class. 

    sealed class Bike : Vehical
    {

        public static void Main(string[] args)
        {
            // Creating object of parent class will automatically invoke
            //   default constructor. 

            Vehical v1 = new Vehical();
            v1.Vehicals("Yamaha", "Blue", 2, 152);
            v1.CalculateTolltax(25);
            Console.ReadLine();
        }



    }


    // Here Car child is inheriting the property of the parent Vehical.
    //   Here, the keyword "sealed" insures that no other class can be derived by this 
    //   using class. 

    sealed class Car : Vehical
    {


        public static void main(string[] args)
        {
            // Creating object of parent class will automatically invoke
            //   default constructor. 

            Vehical v2 = new Vehical();
            v2.Vehicals("POLO", "red", 4, 281);
            v2.CalculateTolltax(2221520);
            Console.ReadLine();
        }
    }

}
