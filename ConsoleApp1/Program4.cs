using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Painter
{

   // Task #1 : In the "Vehicle" class, change the no of wheels to be of enum type.
   // Create an interface called IPainter with Paint method in it and implement it in the 
   // vehicle class.
   // Create another interface for changing the seat cover and implement it abstraclty in
   // child classes.

    public abstract class Ipainter
    {
        public abstract void Paint();
        public abstract SeatCover();
    }

    class Vehical : Ipainter
    {
        private string _name = "";
        string Color = "";
        int WheelNo = 0;
        int MaxSpeed = 0;

        public enum Wheel
        {
            Bike = 2,
            Car = 4
        }

        public override void Paint()
        {
            Console.WriteLine("I am an abstract method being implemented in base class");
        }

        public Vehical()
        {
            Console.WriteLine("Vehical has started...");

        }

        public void CalculateTollTax(int tax)
        {
            Console.WriteLine("Toll Tax for your vehical is: " + tax);
        }

        public void Vehicals(string NewName, string NewColor, int NewWheelNo, int NewMaxSpeed)
        {
            _name = NewName;
            Color = NewColor;
            WheelNo = NewWheelNo;
            MaxSpeed = NewMaxSpeed;

            Console.WriteLine("Name of your vehical is: " + _name);
            Console.WriteLine("Color of your vehical is: " + Color);
            Console.WriteLine("Total wheel in your vehical is : " + WheelNo);
            Console.WriteLine("Maximum speed of your vehical is: " + MaxSpeed);


        }

    }




    class Bike : Vehical
    {

        public static void Main(string[] args)
        {
            // Creating object of parent class will automatically invoke
            //   default constructor.

            Vehical v1 = new Vehical();
            v1.Vehicals("Yamaha", "Blue", 2, 152);
            v1.CalculateTolltax(2563);
            v1.Paint();

       public override void seatcover()
        {
            Console.WriteLine("I am an abstract method being implemented in child class");
        }

        Console.WriteLine("Enter your Vehical category");
        string Category = Console.ReadLine();
        Console.WriteLine("Total number of wheel in your vehical is " + (int) wheel.Bike);
        Console.ReadLine();
        }
}
}
