using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Vehical
    {
        private string _name;
        string Color;
        int WheelNo;
        int MaxSpeed;


        void Start() 
        {
           Console.WriteLine("Vehical has started");
        }

        void Stop()
        {
            Console.WriteLine("Vehical has stopped");
        }

       void SpeedUp(int Speed)
        {
            Console.WriteLine("Speed of vehical has increased by " + Speed );
        }

        


        static void Main(string[] args)
        {

            Vehical v = new Vehical();
           
            v.Start();
            v.Stop();
            Console.WriteLine("Enter speed to Speed up the vehical");
            int Speed = Convert.ToInt32(Console.ReadLine());
            v.SpeedUp(Speed);
            
            Console.WriteLine("Displaying all the information of vehical");
            v._name = "BMW GT 310";
            v.Color = "Blue and red";
            v.WheelNo = 2;
            v.MaxSpeed = 436;

            Console.WriteLine("Namw of the vehical: "+v._name);
            Console.WriteLine("Color of the vehical: "+v.Color);
            Console.WriteLine("Number of wheels in the vehical: "+v.WheelNo);
            Console.WriteLine("Maximum speed of the vehical: "+v.MaxSpeed); 


            Console.ReadLine();

        }
    }
}
