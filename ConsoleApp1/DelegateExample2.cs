using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;


delegate void CustomerStatus(int n);

namespace DelegateApp
{
    class DelegateApp
    {


        public static void OldCustomerOrNot(int orderNumber)
        {

            if (orderNumber <= 1)
            {
                Console.WriteLine("Welcome \n Thank You for shopping with us.");
            }
            else
            {
                Console.WriteLine("Welcome back");
            }

        }





        static void Main(string[] args)
        {
            //create delegate instances
            CustomerStatus ObjCustomerStatus = new CustomerStatus(OldCustomerOrNot);

            Console.WriteLine("Enter the order number of customer");
            int orderNumber = Convert.ToInt32(Console.ReadLine());
            ObjCustomerStatus(orderNumber);
            Console.ReadLine();
        }
    }
}