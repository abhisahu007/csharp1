using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace FileSystem
{
    class Program
    {

        public void sum(int FirstNumber, int SecondNUmber)
        {
            Console.WriteLine(FirstNumber + SecondNUmber);
        }

        public void subtraction(int FirstNumber, int SecondNUmber)
        {
            Console.WriteLine(FirstNumber - SecondNUmber);
        }

        public void divide(int FirstNumber, int SecondNUmber)
        {
            // To handle divide by zero exception
            try
            {
                Console.WriteLine(FirstNumber / SecondNUmber);
            }
            catch (Exception e)
            {
                Console.WriteLine("You can not divide a number by zero");
            }
        }

        public void multi(int FirstNumber, int SecondNUmber)
        {
            try
            {
                Console.WriteLine(FirstNumber * SecondNUmber);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        public static void Main(string[] args)
        {
            Program p = new Program();
            Console.WriteLine("Welcome to Calculator");

            while (true)
            {
                Console.WriteLine("Choose operation");
                Console.WriteLine("Press + for addition");
                Console.WriteLine("Press - for substration");
                Console.WriteLine("Press * for multiplication");
                Console.WriteLine("Press / for division");
                Console.WriteLine("Press any other key to exit");


                try
                {
                    char Choice = Convert.ToChar(Console.ReadLine());
                    switch (Choice)
                    {
                        case '+':
                            Console.WriteLine("Sum operation initiated...");
                            Console.WriteLine("Enter the first number");
                            int FirstNumber = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter the second number");
                            int SecondNUmber = Convert.ToInt32(Console.ReadLine());
                            Console.Write("Sum= ");
                            p.sum(FirstNumber, SecondNUmber);
                            break;

                        case '-':
                            try
                            {
                                Console.WriteLine("Substration operation initiated...");
                                Console.WriteLine("Enter the first number");
                                FirstNumber = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine("Enter the second number");
                                SecondNUmber = Convert.ToInt32(Console.ReadLine());
                                Console.Write("Subtraction = ");
                                p.subtraction(FirstNumber, SecondNUmber);
                            }
                            // To catch overflow of INT 
                            catch (Exception e)
                            {
                                //  Console.WriteLine(e);
                                Console.WriteLine("Please enter the value of INT in a proper range");
                            }
                            break;


                        case '/':
                            try
                            {
                                Console.WriteLine("Divide operation initiated...");
                                Console.WriteLine("Enter the first number");
                                FirstNumber = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine("Enter the second number");
                                SecondNUmber = Convert.ToInt32(Console.ReadLine());
                                Console.Write("Division = ");
                                p.divide(FirstNumber, SecondNUmber);
                            }
                            // To catch overflow of INT 
                            catch (Exception e)
                            {
                                // Console.WriteLine(e);
                                Console.WriteLine("Please enter the value of INT in a proper range");
                            }
                            break;

                        case '*':
                            try
                            {
                                Console.WriteLine("Multiplication operation initiated...");
                                Console.WriteLine("Enter the first number");
                                FirstNumber = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine("Enter the second number");
                                SecondNUmber = Convert.ToInt32(Console.ReadLine());
                                Console.Write("Multiplication = ");
                                p.multi(FirstNumber, SecondNUmber);
                            }
                            // To catch overflow of INT 
                            catch (Exception e)
                            {
                                // Console.WriteLine(e);
                                Console.WriteLine("Please enter the value of INT in a valid range");
                            }
                            break;

                        default:
                            System.Environment.Exit(1);
                            break;
                    }
                }

                // To catch the input type of calculator operation

                catch (Exception e)
                {
                    // Console.WriteLine(e);
                    Console.WriteLine("Press the valid key");
                }
            }
        }
    }
}



