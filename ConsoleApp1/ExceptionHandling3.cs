using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace FileSystem
{

    class Sphere
    {
        public void CalculateSphere(double Radius)
        {
            Console.WriteLine("Surface area of sphere = " + (4 * 3.14 * Radius * Radius));
            Console.WriteLine("Volume of sphere = " + ((4 / 3) * 3.14 * Radius * Radius * Radius));
        }
    }

    class Cube
    {
        public void CalculateCube(double Edge)
        {
            Console.WriteLine("Surface area of cube = " + 6 * Edge * Edge);
            Console.WriteLine("Volume of cube = " + Edge * Edge * Edge);
        }
    }

    class Cylinder
    {
        public void CalculateCylinder(double Radius, double Height)
        {
            Console.WriteLine("Surface area of cylinder = " + (2 * 3.14 * Radius * Height) + (2 * 3.14 * Radius * Radius));
            Console.WriteLine("Volume of the cylinder = " + (3.14 * Radius * Radius * Height));
        }
    }

    class Square
    {
        public void CalculateSquare(double Side)
        {
            Console.WriteLine("Area of Sauare = " + Side * Side);
            Console.WriteLine("Perimeter of square = " + 4 * Side);
        }
    }

    class Rectangle
    {
        public void CalculateRectangle(double Length, double Breath)
        {
            Console.WriteLine("Perimeter of Rectangle = " + 2 * (Length + Breath));
            Console.WriteLine("Area of Reactangle = " + Length * Breath);
        }
    }












    class Program
    {
        public static void Main(string[] args)
        {

            Cube c = new Cube();
            Sphere s = new Sphere();
            Cylinder cylinder = new Cylinder();
            Square sq = new Square();
            Rectangle r = new Rectangle();

            while (true)
            {
                Console.WriteLine("Please select your choice");
                Console.WriteLine("1. Area and Volume of Sphere");
                Console.WriteLine("2. Area and Volume of Cube");
                Console.WriteLine("3. Area and Volume of Cylinder");
                Console.WriteLine("4. Area and Perimeter of Square");
                Console.WriteLine("5. Area and Perimeter of Rectangle");
                Console.WriteLine("6. Exit");

                try
                {
                    int choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            try
                            {
                                Console.WriteLine("Enter radius to calculate area and volume of sphere");
                                double Radius = Convert.ToDouble(Console.ReadLine());
                                s.CalculateSphere(Radius);
                            }

                            catch
                            {
                                Console.WriteLine("Please enter a valid radius");
                            }
                            break;

                        case 2:
                            try
                            {
                                Console.WriteLine("Enter edge of the cube");
                                double Edge = Convert.ToDouble(Console.ReadLine());
                                c.CalculateCube(Edge);
                            }
                            catch
                            {
                                Console.WriteLine("Enter valid edge");
                            }
                            break;

                        case 3:
                            try
                            {
                                Console.WriteLine("Enter height of cylinder");
                                double Height = Convert.ToDouble(Console.ReadLine());
                                Console.WriteLine("Enter the radius of the cylinder");
                                double Radius = Convert.ToDouble(Console.ReadLine());
                                cylinder.CalculateCylinder(Radius, Height);
                            }
                            catch
                            {
                                Console.WriteLine("Enter valid value of radius and height");
                            }
                            break;

                        case 4:
                            try
                            {
                                Console.WriteLine("Enter side of the square");
                                double Side = Convert.ToDouble(Console.ReadLine());
                                sq.CalculateSquare(Side);
                            }
                            catch
                            {
                                Console.WriteLine("Enter a valid Side");
                            }
                            break;

                        case 5:
                            try
                            {
                                Console.WriteLine("Enter length of the rectangle");
                                double Length = Convert.ToDouble(Console.ReadLine());
                                Console.WriteLine("Enter the breath of the rectangle");
                                double Breath = Convert.ToDouble(Console.ReadLine());
                                r.CalculateRectangle(Length, Breath);
                            }
                            catch
                            {
                                Console.WriteLine("Enter value of length and breath in avalid range");
                            }
                            break;

                        default:
                            System.Environment.Exit(1);
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("Please choose a valid option");
                }
            }
        }
    }
}
