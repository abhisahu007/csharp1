using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Task #3 : Create an enum for Months
//           Store the number of days against a month in a Dictionary
//           Create a method which will take the month and will return it days. 

namespace Year
{

    public class Year
    {

        enum Months
        {
            January = 1,
            February,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }

        public static void Query(string NameOfMonth, Dictionary<string, int> Days)
        {
            foreach (KeyValuePair<string, int> Data in Days)
            {
                if (Data.Key == NameOfMonth)
                    Console.WriteLine(Data.Value);
            }
        }

        public static void Main(String[] args)
        {

            Dictionary<String, int> Days = new Dictionary<string, int>();
            Days.Add("January", 31);
            Days.Add("February", 28);
            Days.Add("March", 30);
            Days.Add("April", 31);
            Days.Add("May", 30);
            Days.Add("June", 31);
            Days.Add("July", 30);
            Days.Add("August", 31);
            Days.Add("September", 30);
            Days.Add("October", 31);
            Days.Add("November", 30);
            Days.Add("December", 31);

            Console.WriteLine("Enter name of month to know number of days");
            string NameOfMonth = Console.ReadLine();
            Query(NameOfMonth,Days);

            Console.ReadLine();

        }

    }

}






