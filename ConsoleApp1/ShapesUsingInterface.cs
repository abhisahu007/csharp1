using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace ShapesUsingAbstraction
{



    interface IOneDimension
    {
        void Area(double a);
    }

    interface ITwoDimension
    {
        void Area(double a, double b);
    }




    public class Sphere : IOneDimension
    {
        public void Area(double radius)
        {
            Console.WriteLine("Area of Sphere = " + 4 * 3.14 * radius * radius);
        }
    }

    public class Cube : IOneDimension
    {
        public void Area(double edge)
        {
            Console.WriteLine("Area of cube = " + 6 * edge * edge);
        }
    }

    public class Cylinder : ITwoDimension
    {
        public void Area(double radius, double height)
        {
            Console.WriteLine("Area of Cylinder = " + (2 * 3.14 * radius) * (radius + height));
        }
    }

    public class Square : ITwoDimension 
    {
        public void Area(double side)
        {
            Console.WriteLine("Area of Square = " + side * side);
        }
    }

    public class Rectangle : ITwoDimension
    {
        public void Area(double length, double breath)
        {
            Console.WriteLine("Area of rectangle = " + length * breath);
        }
    }










    class Program
    {
        public static void Main(string[] args)
        {



            while (true)
            {
                Console.WriteLine("Please select your choice");
                Console.WriteLine("1. Area of Sphere");
                Console.WriteLine("2. Area of Cube");
                Console.WriteLine("3. Area of Cylinder");
                Console.WriteLine("4. Area of Square");
                Console.WriteLine("5. Area of Rectangle");
                Console.WriteLine("6. Exit");


                int n;
                bool flag = int.TryParse(Console.ReadLine(), out n);
                if (flag)
                {
                    switch (n)
                    {
                        case 1:

                            Console.WriteLine("Enter radius to calculate area and volume of sphere");
                            double radius;
                            bool tryParseResult;
                            tryParseResult = double.TryParse(Console.ReadLine(), out radius);

                            if (tryParseResult)
                            {
                                Sphere objSphere = new Sphere();
                                objSphere.Area(radius);
                            }
                            else
                            {
                                Console.WriteLine("Radius is not in correct format");
                            }
                            break;

                        case 2:

                            Console.WriteLine("Enter edge of the cube");
                            double edge;
                            tryParseResult = double.TryParse(Console.ReadLine(), out edge);

                            if (tryParseResult)
                            {
                                Cube objCube = new Cube();
                                objCube.Area(edge);
                            }
                            else
                            {
                                Console.WriteLine("Edge is not in correct format");
                            }
                            break;

                        case 3:
                            Console.WriteLine("Enter height of cylinder");
                            double height;
                            tryParseResult = double.TryParse(Console.ReadLine(), out height);

                            Console.WriteLine("Enter the radius of the cylinder");
                            bool tryParseResult2;
                            tryParseResult2 = double.TryParse(Console.ReadLine(), out radius);

                            if (tryParseResult == true && tryParseResult2 == true)
                            {
                                Cylinder objCylinder = new Cylinder();
                                objCylinder.Area(radius, height);
                            }
                            else
                            {
                                Console.WriteLine("Invalid value of Radius or height");
                            }
                            break;

                        case 4:
                            Console.WriteLine("Enter side of the square");
                            double side;
                            tryParseResult = double.TryParse(Console.ReadLine(), out side);

                            if (tryParseResult)
                            {
                                Square objSquare = new Square();
                                objSquare.Area(side);
                            }
                            else
                            {
                                Console.WriteLine("Invalid value of side");
                            }
                            break;

                        case 5:
                            Console.WriteLine("Enter length of the rectangle");
                            double length;
                            tryParseResult = double.TryParse(Console.ReadLine(), out length);
                            Console.WriteLine("Enter the breath of the rectangle");
                            double breath;
                            tryParseResult2 = double.TryParse(Console.ReadLine(), out breath);

                            if (tryParseResult && tryParseResult2)
                            {
                                Rectangle objRectangle = new Rectangle();
                                objRectangle.Area(length, breath);
                            }
                            else
                            {
                                Console.WriteLine("Invalid value of length or width");
                            }
                            break;

                        default:
                            System.Environment.Exit(1);
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Please make a valid selection");
                }

            }
        }
    }
}
