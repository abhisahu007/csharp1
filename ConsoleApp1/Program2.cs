using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Area
{
    class Area
    {


        static void AreaOfReactangle(int Length, int Breath)
        {
            Console.WriteLine("Area of reactangle is: " + Length * Breath);
        }



        static void AreaOfSquare(int Side)
        {
            Console.WriteLine("Area of Square: " + side * side);
        }


        static void AreaOfCircle(int Radius)
        {
            Console.WriteLine("Area of circle: " + 3.14 * Radius * Radius);
        }


        static void Main(string[] args)
        {


            // Calculating area of rectangle

            Console.WriteLine("Enter the length of rectangle");
            int Length = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the breath of rectangle");
            int Breath = Convert.ToInt32(Console.ReadLine());
            AreaOfReactangle(Length, Breath);


            // Calculating area of square

            Console.WriteLine("Enter the length of side of square");
            int Side = Convert.ToInt32(Console.ReadLine());
            AreaOfSquare(Side);


            // Calculting area of circle

            Console.WriteLine("Enter the radius of cirrcle");
            int Radius = Convert.ToInt32(Console.ReadLine());
            AreaOfCircle(Radius);


            Console.ReadLine();

        }
    }
}
