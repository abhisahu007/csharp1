using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;


delegate void CustomerStatus(int n);

namespace AnonymousMethod
{
    class AnonymousMethod
    {

        public static void Main(string[] args)
        {
            int[] numbers = { 4, 9, 2, 3, 4, 5 };
            var squaredNumbers = numbers.Select(x => x * x);
            Console.WriteLine(string.Join(" ", squaredNumbers));
            Console.ReadLine();
        }
    }
}