using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace BookDatabases
{

    class BookDatabase
    {


        public void NewBook(Dictionary<string, string> bookDB)
        {
            Console.WriteLine("Enter title of the book");
            string BookName = Console.ReadLine();
            Console.WriteLine("Enter the author name");
            string AuthorName = Console.ReadLine();
            bookDB.Add(BookName, AuthorName);
            Console.WriteLine("New Book added successfully \n ");
        }


        public void DisplayDB(Dictionary<string, string> bookDB)
        {
            foreach (KeyValuePair<string, string> data in bookDB)
            {
                Console.WriteLine("Book name = {0}  Author name = {1}", data.Key, data.Value);
            }
        }

        public void SearchByTitle(Dictionary<string, string> bookDB)
        {
            Console.WriteLine("Enter title to search");
            string title = Console.ReadLine();
            bool flag = true;

            foreach (KeyValuePair<string, string> data in bookDB)
            {
                if (data.Key == title)
                {
                    Console.WriteLine("Book found");
                    Console.WriteLine("Book name = {0}  Author name = {1}", data.Key, data.Value);
                    flag = false;
                }
            }
            if (flag)
            {
                Console.WriteLine("Record not found in DB \n");
            }
        }


        void SearchByPosition(Dictionary<string, string> bookDB)
        {
            Console.WriteLine("Enter the position of book to be deleted");
            int position = Convert.ToInt32(Console.ReadLine());
            int index = 1;

            foreach (KeyValuePair<string, string> data in bookDB)
            {
                if (index == position)
                {
                    Console.WriteLine("Book Deleted");
                    bookDB.Remove(data.Key);
                    break;
                }
                index++;
            }
        }



        public static void Main(string[] args)
        {
            Dictionary<string, string> bookDB = new Dictionary<string, string>();
            BookDatabase d = new BookDatabase();

            while (true)
            {
                Console.WriteLine("Please choose an option");
                Console.WriteLine("1. Add a new book");
                Console.WriteLine("2. Display all the book");
                Console.WriteLine("3. Search for the book with certain title");
                Console.WriteLine("4. Delete a book by position");
                Console.WriteLine("5. EXIT \n");

                int choice;
                bool tryParseResult = int.TryParse(Console.ReadLine(), out choice);

                if (tryParseResult)
                {
                    switch (choice)
                    {
                        case 1:

                            d.NewBook(bookDB);
                            break;

                        case 2:
                            d.DisplayDB(bookDB);
                            break;

                        case 3:
                            d.SearchByTitle(bookDB);
                            break;

                        case 4:
                            d.SearchByPosition(bookDB);
                            break;

                        default:
                            System.Environment.Exit(1);
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Please choose a valid option");
                }
            }
        }
    }
}