﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("SELECT YOUR OPTION");
                Console.WriteLine("PRESS 1 for Task 1");
                Console.WriteLine("PRESS 2 for Task 2");
                Console.WriteLine("PRESS 3 for Task 3");
                Console.WriteLine("PRESS 4 for EXIT");


                var a = 0;
                a = Convert.ToInt32(Console.ReadLine());
                switch (a)
                {
                    case 1:
                        {
                            Console.WriteLine("Task 1 Selected");
                            string[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
                            Console.WriteLine("An array has been created");
                            Console.WriteLine("Now printing Month which starts with J using for loop");

                            for (int i = 0; i < months.Length; i++)
                            {
                                Console.WriteLine(months[i]);
                            }
                            Console.WriteLine("Now printing Month which starts with J using forEach loop");

                            foreach (string element in months)
                            {
                                Console.WriteLine(element);
                            }

                            Console.WriteLine("Now printing Month which starts with J using for while loop");

                            var p = 0;
                            while (p < months.Length)
                            {
                                Console.WriteLine(months[p]);
                                p++;
                            }

                            break;
                        }





                    case 2:
                        {
                            Console.WriteLine("Task 2 Selected");
                            Console.WriteLine("Using the switch statement, find out if the color passed is either Red, Green or Blue and print out the message accordingly.");
                            Console.WriteLine("Enter color as red or green or blue");
                            string color = Console.ReadLine();

                            switch (color)
                            {

                                case "red":
                                    {
                                        Console.WriteLine("Color in in RGB");
                                        break;
                                    }
                                case "blue":
                                    {
                                        Console.WriteLine("Color in in RGB");
                                        break;
                                    }
                                case "green":
                                    {
                                        Console.WriteLine("Color in in RGB");
                                        break;
                                    }
                                default:
                                    {
                                        Console.WriteLine("Color is not in RGB");
                                        break;
                                    }
                            }

                            Console.WriteLine("Using if statement");
                            if (color == "blue" || color == "green" || color == "red") Console.WriteLine("Entered color is in RGB");
                            else Console.WriteLine("Entered color is not in RGB");


                            Console.WriteLine("Using Trenary operator");

                            var result = color == "red" ? "Color is in RGB" : "Color is in not in RGB";
                            Console.WriteLine(result);
                            break;
                        }
                    case 3:
                        {
                            Console.WriteLine("I am 3");
                            break;
                        }
                    default:
                        {
                            System.Environment.Exit(1);
                            break;
                        }

                }
            }
        }
    }
}
