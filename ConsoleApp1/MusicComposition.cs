using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace MusicalComposition
{
    public class MusicalComposition
    {
        string title, composer;
        int year;

        public MusicalComposition(string title, string composer, int year)
        {
            this.title = title;
            this.composer = composer;
            this.year = year;

            Console.WriteLine("Displaying music label details:");
            Console.WriteLine("Title of music = " + title);
            Console.WriteLine("Composer of music = " + composer);
            Console.WriteLine("Year of release = " + year);

        }
    }


    public class NationalAnthem : MusicalComposition
    {
        string AnthemNation;

        public NationalAnthem(string AnthemNation, string title, string composer, int year) : base(title, composer, year)
        {
            this.AnthemNation = AnthemNation;
            // Console.WriteLine("...Display from the NationalAnthem...");
            Console.WriteLine("Anthem Nation = " + AnthemNation);
            /* Console.WriteLine("Title of music = "+ title);
             Console.WriteLine("Composer of music = "+ composer);
             Console.WriteLine("Year of release = "+ year); */
        }
    }


    class Program
    {
        static void Main(String[] args)
        {
            Console.WriteLine("Enter title of the music");
            string title = Console.ReadLine();
            Console.WriteLine("Enter the composer of music");
            string composer = Console.ReadLine();
            Console.WriteLine("Enter the year of release of music");
            int year = Int32.Parse(Console.ReadLine());
            // MusicalComposition m = new MusicalComposition(title,composer,year);

            Console.WriteLine("Enter the Anthem's nation");
            string anth = Console.ReadLine();
            NationalAnthem na = new NationalAnthem(anth, title, composer, year);


            Console.ReadLine();
        }
    }

}
