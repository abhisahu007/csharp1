using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Party_Management_System
{

    public class Party
    {
        public string BookingType;
        public int NoOfPeople;
        public string PartyVenue;
        public double DecorationCharges;
        public double CateringCharges;

        public virtual void PartyPackage(string PartyVenue)
        {

        }

        public virtual void DisplayPartyDetails(string PackageName, double DecorationCharges, double CateringCharges)
        {

        }

    }

    class Birthday : Party
    {
        int ReadVenue;
        int ReadPackage;
        string BookingDate;
        int BookingId;
        public void BirthdayOrganizing(int TotalPeople, string Date, int Value)
        {
            BookingDate = Date;
            BookingId = Value;
            base.NoOfPeople = TotalPeople;
            Console.WriteLine(" <--------------------> Welcome To Birthday Booking System <-------------------->");
            Console.WriteLine("Venue Available : --->");
            Console.WriteLine("1. Taj Ganges");
            Console.WriteLine("2. Pallassio");
            ReadVenue = Convert.ToInt32(Console.ReadLine());
            switch (ReadVenue)
            {
                case 1:
                    base.PartyVenue = "Taj Ganges";
                    PartyPackage(base.PartyVenue);
                    break;
                case 2:
                    base.PartyVenue = "Pallassio";
                    PartyPackage(base.PartyVenue);
                    break;
                default:
                    Console.WriteLine("Entered Wrong Choice ! Please Select Again :");
                    break;
            }

        }

        public override void PartyPackage(string PartyVenue)
        {
            string CheckCondition = "n";
            do
            {
                Console.WriteLine("Select Packages : ");
                Console.WriteLine("1. Silver Package ");
                Console.WriteLine("2. Gold Package : ");
                Console.WriteLine("3. Platinum Package : ");
                ReadPackage = Convert.ToInt32(Console.ReadLine());

                switch (ReadPackage)
                {

                    case 1:
                        SilverPackage();
                        break;
                    case 2:
                        GoldPackage();
                        break;
                    case 3:
                        PlatinumPackage();
                        break;
                    default:
                        Console.WriteLine("Entered Wrong Choice ! Enter [y/Y] to Select choice again :");
                        CheckCondition = Console.ReadLine();
                        break;
                }
            } while (CheckCondition == "y" || CheckCondition == "Y");
            void SilverPackage()
            {
                string PackageSilver = "Silver Package";
                base.DecorationCharges = 29000;
                base.CateringCharges = 19000;
                DisplayPartyDetails(PackageSilver, base.DecorationCharges, base.CateringCharges);

            }
            void GoldPackage()
            {
                string PackageGold = "Gold Package";
                base.DecorationCharges = 11000;
                base.CateringCharges = 21000;
                DisplayPartyDetails(PackageGold, base.DecorationCharges, base.CateringCharges);
            }
            void PlatinumPackage()
            {
                string PackagePlatinum = "Platinum Package";
                base.DecorationCharges = 14999;
                base.CateringCharges = 299000;
                DisplayPartyDetails(PackagePlatinum, base.DecorationCharges, base.CateringCharges);
            }

        }
        
        public override void DisplayPartyDetails(string PackageName, double DecorationCharges, double CateringCharges)
        {
            if (PackageName == "Silver Package")
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Birthday Party");
                Console.WriteLine("Booking ID : " + BookingId);
                Console.WriteLine("Package : " + PackageName);
                Console.WriteLine("Venue : " + base.PartyVenue);
                Console.WriteLine("Booking Date : " + BookingDate);
                double TotalCost = DecorationCharges + CateringCharges;
                Console.WriteLine("Cost Per Person :" + TotalCost / base.NoOfPeople);
                Console.WriteLine("Includes Unlimited Buffe and Extra One Diffrent Snacks");
                Console.WriteLine();
            }
            else if (PackageName == "Gold Package")
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Birthday Party");
                Console.WriteLine("Booking ID : " + BookingId);
                Console.WriteLine("Package : " + PackageName);
                Console.WriteLine("Venue : " + base.PartyVenue);
                Console.WriteLine("Booking Date : " + BookingDate);
                double TotalCost = DecorationCharges + CateringCharges;
                Console.WriteLine("Cost Per Person :" + TotalCost / base.NoOfPeople);
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("Booking is Confirmed");
                Console.WriteLine("Party Type : Birthday Party");
                Console.WriteLine("Booking ID : " + BookingId);
                Console.WriteLine("Package : " + PackageName);
                Console.WriteLine("Venue : " + base.PartyVenue);
                Console.WriteLine("Booking Date : " + BookingDate);
                double TotalCost = DecorationCharges + CateringCharges;
                Console.WriteLine("Cost Per Person :" + TotalCost / base.NoOfPeople);
                Console.WriteLine("Free Goodies");
            }
        }
    }
    class Anniversary : Party
    {
        int ReadVenue;
        int ReadPackage;
        string BookingDate;
        int BookingId;
        public void AnniversaryOrganizing(int totalPeople, string date, int value)
        {
            BookingDate = date;
            BookingId = value;
            base.NoOfPeople = totalPeople;
            Console.WriteLine("AnNiVeRsArY Booking System");
            Console.WriteLine("--- Venue Available : ---");
            Console.WriteLine("1. Taj Ganges");
            Console.WriteLine("2. Pallassio");
            ReadVenue = Convert.ToInt32(Console.ReadLine());
            switch (ReadVenue)
            {
                case 1:
                    base.PartyVenue = "Taj Ganges";
                    PartyPackage(base.partyVenue);
                    break;
                case 2:
                    base.PartyVenue = "Pallassio";
                    PartyPackage(base.partyVenue);
                    break;
                default:
                    Console.WriteLine("Wrong Choice... Try Again");
                    break;
            }

        }

        public override void PartyPackage(string partyVenue)
        {
            Console.WriteLine("Select Packages : ");
            Console.WriteLine("1. Silver Package ");
            Console.WriteLine("2. Gold Package : ");
            Console.WriteLine("3. Platinum Package : ");
            ReadPackage = Convert.ToInt32(Console.ReadLine());

            switch (ReadPackage)
            {
                case 1:
                    SilverPackage();
                    break;
                case 2:
                    GoldPackage();
                    break;
                case 3:
                    PlatinumPackage();
                    break;
                default:
                    Console.WriteLine("Wrong Choice... Try Again");
                    break;
            }

            void SilverPackage()
            {
                string PackageSilver = "Silver Package";
                base.DecorationCharges = 15000;
                base.CateringCharges = 50000;
                DisplayPartyDetails(PackageSilver, base.DecorationCharges, base.CateringCharges);

            }
            void GoldPackage()
            {
                string PackageGold = "Gold Package";
                base.DecorationCharges = 20000;
                base.CateringCharges = 75000;
                DisplayPartyDetails(PackageGold, base.DecorationCharges, base.CateringCharges);
            }
            void PlatinumPackage()
            {
                string PackagePlatinum = "Platinum Package";
                base.DecorationCharges = 30000;
                base.CateringCharges = 100000;
                DisplayPartyDetails(PackagePlatinum, base.DecorationCharges, base.CateringCharges);
            }

        }
        public override void DisplayPartyDetails(string PackageName, double DecorationCharges, double CateringCharges)
        {
            if (PackageName == "Silver Package" && NoOfPeople <= 150)
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Anniversary Party");
                Console.WriteLine("Booking ID : " + BookingId);
                Console.WriteLine("Package : " + PackageName);
                Console.WriteLine("Venue : " + base.PartyVenue);
                Console.WriteLine("Booking Date : " + BookingDate);
                double TotalCost = DecorationCharges + CateringCharges;
                Console.WriteLine("Cost Per Person :" + TotalCost / base.NoOfPeople);
                Console.WriteLine("Includes Unlimited Buffe and Extra One Diffrent Snacks");
                Console.WriteLine("Maximum 150 Peoples Allowed");
                Console.WriteLine();
            }
            else if (PackageName == "Gold Package" && NoOfPeople > 150 && NoOfPeople <= 250)
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Anniversary Party");
                Console.WriteLine("Booking ID : " + BookingId);
                Console.WriteLine("Package : " + PackageName);
                Console.WriteLine("Venue : " + base.PartyVenue);
                Console.WriteLine("Booking Date : " + BookingDate);
                double TotalCost = DecorationCharges + CateringCharges;
                Console.WriteLine("Cost Per Person :" + TotalCost / base.NoOfPeople);
                Console.WriteLine("Includes Unlimited Buffe   Mocktails and Extra One Diffrent Snacks");
                Console.WriteLine("Gather upto 250 peoples");
                Console.WriteLine();
            }
            else
            {
                Console.WriteLine("--- Booking is Confirmed ---");
                Console.WriteLine("Party Type : Anniversary Party");
                Console.WriteLine("Booking ID : " + BookingId);
                Console.WriteLine("Package : " + PackageName);
                Console.WriteLine("Venue : " + base.PartyVenue);
                Console.WriteLine("Booking Date : " + BookingDate);
                double TotalCost = DecorationCharges + CateringCharges;
                Console.WriteLine("Cost Per Person :" + TotalCost / base.NoOfPeople);
                Console.WriteLine("Includes Unlimited Buffe   Mocktails   Cocktails and Extra Two Diffrent Snacks");
                Console.WriteLine("Free Parking Facility");
                Console.WriteLine("Gather upto 500 peoples");
                Console.WriteLine();
            }
        }
    }


    public class Program
    {
        static void Main(string[] args)
        {
            string Condition;
            DateTime Now = DateTime.Now;
            Random Rnd = new Random();
            int Value;
            string CurrentDate = now.ToString("F");
            do
            {
                Console.WriteLine("*******WELCOME TO SUCCESSIVE PARTY MANAGEMENT SYSTEM*******");
                Console.WriteLine();
                Console.WriteLine("Select your Booking Category");
                Console.WriteLine("1. Birthday Party");
                Console.WriteLine("2. Anniversary Party");
                int BookingType = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                Birthday bObje = new Birthday();
                Anniversary anObj = new Anniversary();

                int NoOfPeople;
                switch (BookingType)
                {
                    case 1:
                        Console.WriteLine("Enter Number of Peoples");
                        NoOfPeople = Convert.ToInt32(Console.ReadLine());
                        Value = rnd.Next(100000, 999999);
                        bObje.BirthdayOrganizing(NoOfPeople, CurrentDate, Value);
                        break;
                    case 2:
                        Console.WriteLine("Enter Number of Peoples");
                        NoOfPeople = Convert.ToInt32(Console.ReadLine());
                        Value = rnd.Next(100000, 999999);
                        anObj.AnniversaryOrganizing(NoOfPeople, CurrentDate, Value);
                        break;
                    default:
                        Console.WriteLine("Wrong Choice Selected ! Please Select Again :");
                        break;
                }
                Console.WriteLine("Thank You for choosing us");
                Console.WriteLine("Press Y/y to countinue or N/n to exit.");
                Condition = Console.ReadLine();
            }
            while (Condition == "y" || Condition == "Y");
            Console.ReadLine();
        }
    }
}
