using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace MultipleInterfaceWithSameName
{   
    // Task 1: Same name function in two different interface, calling in main method 

    interface ISquare
    {
        void Area();
    }

    interface IRectangle
    {
        void Area();
    }

    class Calculator : ISquare, IRectangle
    {
        void ISquare.Area()
        {
            Console.WriteLine("I am Area from Square interface");
        }

        void IRectangle.Area()
        {
            Console.WriteLine("I am Area from Rectangle Interface");
        }
    }

    class Program
    {
        public static void Main(string[] args)
        {
            ISquare s = new Calculator();
            s.Area();

            IRectangle r = new Calculator();
            r.Area();

            Console.ReadLine();
        }
    }
}
