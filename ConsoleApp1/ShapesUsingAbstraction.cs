using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace ShapesUsingAbstraction
{



    public abstract class OneDimension
    {
        abstract public void Area(double side);
    }

    public abstract class TwoDimension
    {
        abstract public void Area(double length, double width);
    }




    public class Sphere : OneDimension
    {
        public override void Area(double radius)
        {
            Console.WriteLine("Area of Sphere = " + 4 * 3.14 * radius * radius);
        }
    }

    public class Cube : OneDimension'
    {
        public override void Area(double edge)
        {
            Console.WriteLine("Area of cube = " + 6 * edge * edge);
        }
    }

    public class Cylinder : TwoDimension
    {
        public override void Area(double radius, double height)
        {
            Console.WriteLine("Area of Cylinder = " + (2 * 3.14 * radius) * (radius + height));
        }
    }

    public class Square : OneDimension
    {
        public override void Area(double side)
        {
            Console.WriteLine("Area of Square = " + side * side);
        }
    }

    public class Rectangle : TwoDimension
    {
        public override void Area(double length, double breath)
        {
            Console.WriteLine("Area of rectangle = " + length * breath);
        }
    }










    class Program
    {
        public static void Main(string[] args)
        {



            while (true)
            {
                Console.WriteLine("Please select your choice");
                Console.WriteLine("1. Area of Sphere");
                Console.WriteLine("2. Area of Cube");
                Console.WriteLine("3. Area of Cylinder");
                Console.WriteLine("4. Area of Square");
                Console.WriteLine("5. Area of Rectangle");
                Console.WriteLine("6. Exit");

                try
                {
                    int choice = Convert.ToInt32(Console.ReadLine());
                    switch (choice)
                    {
                        case 1:
                            try
                            {
                                Console.WriteLine("Enter radius to calculate area and volume of sphere");
                                double radius = Convert.ToDouble(Console.ReadLine());
                                Sphere objSphere = new Sphere();
                                objSphere.Area(radius);
                            }

                            catch
                            {
                                Console.WriteLine("Please enter a valid radius");
                            }
                            break;

                        case 2:
                            try
                            {
                                Console.WriteLine("Enter edge of the cube");
                                double edge = Convert.ToDouble(Console.ReadLine());
                                Cube objCube = new Cube();
                                objCube.Area(edge);
                            }
                            catch
                            {
                                Console.WriteLine("Enter valid edge");
                            }
                            break;

                        case 3:
                            try
                            {
                                Console.WriteLine("Enter height of cylinder");
                                double height = Convert.ToDouble(Console.ReadLine());
                                Console.WriteLine("Enter the radius of the cylinder");
                                double radius = Convert.ToDouble(Console.ReadLine());
                                Cylinder objCylinder = new Cylinder();
                                objCylinder.Area(radius, height);
                            }
                            catch
                            {
                                Console.WriteLine("Enter valid value of radius and height");
                            }
                            break;

                        case 4:
                            try
                            {
                                Console.WriteLine("Enter side of the square");
                                double side = Convert.ToDouble(Console.ReadLine());
                                Square objSquare = new Square();
                                objSquare.Area(side);
                            }
                            catch
                            {
                                Console.WriteLine("Enter a valid Side");
                            }
                            break;

                        case 5:
                            try
                            {
                                Console.WriteLine("Enter length of the rectangle");
                                double length = Convert.ToDouble(Console.ReadLine());
                                Console.WriteLine("Enter the breath of the rectangle");
                                double breath = Convert.ToDouble(Console.ReadLine());
                                Rectangle objRectangle = new Rectangle();
                                objRectangle.Area(length, breath);
                            }
                            catch
                            {
                                Console.WriteLine("Enter value of length and breath in avalid range");
                            }
                            break;

                        default:
                            System.Environment.Exit(1);
                            break;
                    }
                }
                catch
                {
                    Console.WriteLine("Please choose a valid option");
                }
            }
        }
    }
}
