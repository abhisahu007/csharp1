using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Party_Management_System
{
    // Task :: Exception Handling : 
    //         Divide by Zero Exception    

    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("We are handling Divide by Zero Exception");
            Console.WriteLine("Enter first number");
            int FirstNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter second number");
            int SecondNumber = Convert.ToInt32(Console.ReadLine());

            try
            {
                Console.WriteLine("Division of both number = " + FirstNumber / SecondNumber);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine("I am a custom message from the catch block");
            }

            finally
            {
                Console.WriteLine("I am finnaly, i don't care if exception is handelled or not");
            }

            Console.ReadLine();

        }
    }
}