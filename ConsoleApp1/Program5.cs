using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Task #2 : Create a class called "Person" with two properties, Name and Age.
// Create a List of Person type and populate it with 5 sample persons
// Display all the name of the persons along with age whose age is greater 
// than 60 for example.

namespace Person
{

    public class Person
    {

        public static void Main(string[] args)
        {
            Dictionary<string, int> Persons = new Dictionary<string, int>();
            Persons.Add("Abhi", 61);
            Persons.Add("Praveen", 71);
            Persons.Add("Harshit", 24);
            Persons.Add("Shubham", 41);
            Persons.Add("Aman", 29);

            Console.WriteLine("Displaying all the data of List whose age is greater than 60");
            foreach (KeyValuePair<string, int> Data in Persons)
            {
                if (Data.Value > 60)
                {
                    Console.WriteLine(data.Key + " " + data.Value);

                }

            }

            Console.ReadLine();
        }

    }

}
