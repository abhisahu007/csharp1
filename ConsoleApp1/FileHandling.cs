using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace FileSystem
{



    class Program
    {

        public static void Main(string[] args)
        {
            FileStream fs = new FileStream("c:\\test.txt", FileMode.Append, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);
            Console.WriteLine("Enter the text which you want to write to the file");
            string str = Console.ReadLine();
            sw.WriteLine(str);
            sw.Flush();
            sw.Close();
            fs.Close();
            Console.WriteLine("Press a key to read the file");
            Console.ReadLine();



            fs = new FileStream("c:\\test.txt", FileMode.Open, FileAccess.Read);
            StreamReader sr = new StreamReader(fs);
            Console.WriteLine("C# File system Read mode initiate...");
            sr.BaseStream.Seek(0, SeekOrigin.Begin);
            string str2 = sr.ReadLine();
            while (str2 != null)
            {
                Console.WriteLine(str2);
                str2 = sr.ReadLine();
            }
            Console.ReadLine();
            sr.Close();
            fs.Close();
            Console.ReadLine();


        }
    }

}
