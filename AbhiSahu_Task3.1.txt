MS SQL Function examples

1.Count function - To count the number of entity.
  SELECT COUNT(EmpName) FROM Employee;

2. Distinct function - To select unique values only
  SELECT DISTINCT EmpID FROM Employee;

3. MAX function - To return maximum from a column.
   SELECT MAX(BONUS_AMOUNT) FROM Bonus;

4. MIN function - To return minimum from a column. 
   SELECT MIN(BONUS_AMOUNT) FROM BONUS;

5. AVG function - To retuen average of the column.
   SELECT AVG(BONUS_AMOUNT) from BONUS;

6. CONCAT function - To combine two strings together
   SELECT CONCAT(FIRSTNAME,LASTNAME) FROM Employeel

7. UPPER function - TO convert the values to Uppercase.
   SELECT UPPER(EmpName) from Employee;

8. LOWER function - To convert the value to Lowercase. 
   SELECT LOWER(EmpName) from Employee;   

9. REVERSE function - To reverse the string
   SELECT REVERSE(EmpName) from Employee;

10. RTRIM function - Remove extra whitespaces form the Right  
    SELECT RTRIM(PASSWORD) FROM Employee;

11. LTRIM function - Remove extra whitespaces form the Left 
    SELECT LTRIM(PASSWORD) FROM LTRIM;

12. TRIM function - Remove extra whitespaces form the Right and Left both. 
    SELECT TRIM(PASSWORD) FROM LTRIM;

13. SUM function - Retuen sum of values passed 
    SELECT SUM(SALRY) FROM Employee;

14. FLOOR function - Return floor value of the nunmber 
    SELECT FLOOR(Temperature) FROM Weather;

15. ABS function 
    SELECT ABS(Temperature) FROM Weather;      